﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using lab6_1;

namespace Test_S
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Silnia_0()
        {
            Silnia S0 = new Silnia();
            S0.n = 0;
            Assert.AreEqual(S0.oblicz(), 1);
        }

        [TestMethod]
        public void Silnia_1()
        {
            Silnia S0 = new Silnia();
            S0.n = 1;
            Assert.AreEqual(S0.oblicz(), 1);
        }

        [TestMethod]
        public void Silnia_5()
        {
            Silnia S0 = new Silnia();
            S0.n = 5;
            Assert.AreEqual(S0.oblicz(), 120);
        }
    }
}
